package hu.adapter.restadapter.entity;

public class Film {
//    {
//        title: "180",
//                release_year: "2011",
//            locations: "Justin Herman Plaza",
//            production_company: "SPI Cinemas",
//            director: "Jayendra",
//            writer: "Umarji Anuradha, Jayendra, Aarthi Sriram, & Suba ",
//            actor_1: "Siddarth",
//            actor_2: "Nithya Menon",
//            actor_3: "Priya Anand"
//    }

    String title;
    String release_year;
    String locations;
    String production_company;
    String director;
    String writer;
    String actor_1;
    String actor_2;
    String actor_3;

    public Film() {
    }

    public Film(String title, String release_year, String locations, String production_company, String director, String writer, String actor_1, String actor_2, String actor_3) {
        this.title = title;
        this.release_year = release_year;
        this.locations = locations;
        this.production_company = production_company;
        this.director = director;
        this.writer = writer;
        this.actor_1 = actor_1;
        this.actor_2 = actor_2;
        this.actor_3 = actor_3;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRelease_year() {
        return release_year;
    }

    public void setRelease_year(String release_year) {
        this.release_year = release_year;
    }

    public String getLocations() {
        return locations;
    }

    public void setLocations(String locations) {
        this.locations = locations;
    }

    public String getProduction_company() {
        return production_company;
    }

    public void setProduction_company(String production_company) {
        this.production_company = production_company;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getActor_1() {
        return actor_1;
    }

    public void setActor_1(String actor_1) {
        this.actor_1 = actor_1;
    }

    public String getActor_2() {
        return actor_2;
    }

    public void setActor_2(String actor_2) {
        this.actor_2 = actor_2;
    }

    public String getActor_3() {
        return actor_3;
    }

    public void setActor_3(String actor_3) {
        this.actor_3 = actor_3;
    }

    @Override
    public String toString() {
        return "Film{" +
                "title='" + title + '\'' +
                ", release_year='" + release_year + '\'' +
                ", locations='" + locations + '\'' +
                ", production_company='" + production_company + '\'' +
                ", director='" + director + '\'' +
                ", writer='" + writer + '\'' +
                ", actor_1='" + actor_1 + '\'' +
                ", actor_2='" + actor_2 + '\'' +
                ", actor_3='" + actor_3 + '\'' +
                '}';
    }
}
