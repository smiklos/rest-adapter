package hu.adapter.restadapter.restcontroller;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import hu.adapter.restadapter.entity.Film;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class RestAdapterController {

    ObjectMapper mapper;

    @Autowired
    public RestAdapterController(ObjectMapper mapper) {
        this.mapper = mapper;
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
    }

    @GetMapping("/status")
    String status() {
        return "OK!";
    }

    @GetMapping("/film")
    String film() throws IOException {
        return getFilms();
    }
    @GetMapping("/film/{searchTerm}")
    String filmFilter(@PathVariable String searchTerm) throws IOException {
        List<Film> films = getfilmObjectList();
        List<Film> filteredFilms = films.stream().filter(x -> x.toString().contains(searchTerm)).collect(Collectors.toList());
        return  mapper.writeValueAsString(filteredFilms);
    }

    String getFilms() throws IOException {
        URL urlForGetRequest = new URL("https://data.sfgov.org/resource/wwmu-gmzc.json");
        String readLine = null;
        HttpURLConnection conection = (HttpURLConnection) urlForGetRequest.openConnection();
        conection.setRequestMethod("GET");
        int responseCode = conection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(conection.getInputStream()));
            StringBuffer response = new StringBuffer();
            while ((readLine = in .readLine()) != null) {
                response.append(readLine);
            } in .close();
           return response.toString();
        } else {
            return "" + responseCode;
        }
    }

    List<Film> getfilmObjectList() throws IOException {
        String films = getFilms();
        List<Film> myObjects = mapper.readValue(films, new TypeReference<List<Film>>(){});
        return  myObjects;
    }
}
